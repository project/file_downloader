<?php

namespace Drupal\file_downloader;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Download option config entities.
 *
 * @see AdminHtmlRouteProvider
 * @see DefaultHtmlRouteProvider
 */
class DownloadOptionConfigHtmlRouteProvider extends AdminHtmlRouteProvider {

}

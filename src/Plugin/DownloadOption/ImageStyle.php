<?php

namespace Drupal\file_downloader\Plugin\DownloadOption;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\FileInterface;
use Drupal\file_downloader\Annotation\DownloadOption;
use Drupal\file_downloader\DownloadOptionPluginBase;
use Drupal\file_downloader\Entity\DownloadOptionConfigInterface;
use Drupal\image\ImageStyleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a download option plugin.
 *
 * @DownloadOption(
 *   id = "image_style",
 *   label = @Translation("Image Style"),
 *   description = @Translation("Download a file based on a image style."),
 * )
 */
class ImageStyle extends DownloadOptionPluginBase {

  /**
   * Image Style storage object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private \Drupal\Core\Entity\EntityStorageInterface $imageStyleStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $fileSystem, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $fileSystem
    );
    $this->imageStyleStorage = $entityTypeManager->getStorage('image_style');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getHeaders(FileInterface $file, DownloadOptionConfigInterface $downloadOptionConfig): array {
    $headers = parent::getHeaders($file, $downloadOptionConfig);
    $headers['Content-Length'] = filesize($this->getFileUri($file));

    return $headers;
  }

  /**
   * @inheritdoc
   */
  public function downloadOptionForm(array $form, FormStateInterface $form_state): array {
    $styles = $this->imageStyleStorage->loadMultiple();

    $options = [];
    foreach ($styles as $name => $style) {
      $options[$name] = $style->get('label');
    }

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Style'),
      '#default_value' => $this->getConfigurationValue('image_style'),
      '#options' => $options,
      '#required' => TRUE,
      '#empty_option' => $this->t('Please select a image style.'),
    ];

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function defaultConfiguration(): array {
    return [
      'id' => $this->getPluginId(),
      'extensions' => '',
      'image_style' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function downloadOptionSubmit($form, FormStateInterface $form_state) {
    $this->configuration['image_style'] = $form_state->getValue('image_style');
  }

  /**
   * {@inheritdoc}
   */
  public function downloadFileExists(FileInterface $file): bool {
    if (!parent::downloadFileExists($file)) {
      return FALSE;
    }

    return $this->imageStyleFileExists($file);
  }

  /**
   * Validate if the image style could be created.
   *
   * @param \Drupal\file\FileInterface $file
   *
   * @return bool
   */
  private function imageStyleFileExists(FileInterface $file): bool {
    $image_style_id = $this->getConfigurationValue('image_style');

    if (empty($image_style_id)) {
      return FALSE;
    }

    /** @var ImageStyleInterface $image_style */
    $image_style = $this->imageStyleStorage->load($image_style_id);
    $image_style_uri = $image_style->buildUri($file->getFileUri());

    $status = file_exists($image_style_uri);
    if (!$status) {
      $image_style->createDerivative($file->getFileUri(), $image_style_uri);
      $status = file_exists($image_style_uri);
    }

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileUri(FileInterface $file): string {
    $image_style_id = $this->getConfigurationValue('image_style');
    /** @var ImageStyleInterface $image_style */
    $image_style = $this->imageStyleStorage->load($image_style_id);
    return $image_style->buildUri($file->getFileUri());
  }

}
